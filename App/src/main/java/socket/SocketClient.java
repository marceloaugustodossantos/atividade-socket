/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package socket;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Properties;

public class SocketClient {
    
    private Socket socket;
    
    public SocketClient() throws IOException{
        this.socket = new Socket( "localhost" , 10103 );
    }
    
    public String request(String msg) throws IOException{       
        this.socket.getOutputStream().write(  msg.getBytes() );
        this.socket.getOutputStream().flush();
        this.socket.shutdownOutput();
        byte[] buffer = new byte[1024];     
        this.socket.getInputStream().read(buffer);
        int aux = 0;
        for(int i = 0; i < buffer.length; i++){
            if(buffer[i] != 0){
                aux++;
            }
        }
        byte[] bufferAux = new byte[aux];
        System.arraycopy(buffer, 0, bufferAux, 0, bufferAux.length);
        this.socket.shutdownInput();       
        return new String(bufferAux);
    }
    
    public void close() throws IOException{
        this.socket.close();
    }
    
}
