package dao;

import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import objectValues.Estoque;

public class DaoEstoque {

    Conexao conexao = new Conexao();

    public List<Estoque> listarEstoque() {
        try {
            Connection c = conexao.getConnection();
            Statement stat = c.createStatement();
            ResultSet rs = stat.executeQuery("select * from tbstock");
            List<Estoque> listaEstoque = new ArrayList();
            while (rs.next()) {
                Estoque estoque = new Estoque();
                estoque.setId(rs.getInt("code"));
                estoque.setNome(rs.getString("name"));
                estoque.setQuantidade(rs.getDouble("quantity"));
                estoque.setValor(rs.getDouble("price"));
                listaEstoque.add(estoque);
            }

            stat.close();
            c.close();
            return listaEstoque;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public boolean cadastrarProduto(String nome, double valor, int qtd){
        try {
            Connection c = conexao.getConnection();
            PreparedStatement stat = c.prepareStatement("INSERT INTO tbstock (name, price, quantity) VALUES (?, ?, ?)");
            stat.setString(1, nome);
            stat.setDouble(2, valor);
            stat.setInt(3, qtd);
            int update = stat.executeUpdate();
            stat.close();
            c.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public int getQtdAtualizada(int id) {
        try {
            Connection c = conexao.getConnection();
            Statement stat = c.createStatement();
            ResultSet rs = stat.executeQuery("select quantity from tbstock where code = "+id);
            int qtd =0;
            if (rs.next()) {
                qtd = rs.getInt("quantity");
            }
            stat.close();
            c.close();
            return qtd;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0; 
    }
    
}
