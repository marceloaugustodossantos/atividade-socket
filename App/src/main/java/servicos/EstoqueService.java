/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicos;

import dao.DaoEstoque;
import java.util.List;
import objectValues.Estoque;
import socket.SocketClient;

/**
 *
 * @author Marcelo Augusto
 */
public class EstoqueService {

    DaoEstoque dao = new DaoEstoque();

    public void cadastrarProduto(String nome, double valor, int qtd) {
        dao.cadastrarProduto(nome, valor, qtd);
    }

    public List<Estoque> listarEstoque() {
        try {
            SocketClient socket = new SocketClient();
            String retorno = socket.request("listAll");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dao.listarEstoque();
    }

    public int getQtdAtualizada(int id) {
        return dao.getQtdAtualizada(id);
    }

}
