/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicos;

import java.io.IOException;
import socket.SocketClient;

/**
 *
 * @author Marcelo Augusto
 */
public class CaixaService {
    
    
    public boolean vender (int id, int qtd, double valor) throws IOException{
        String valorTotal = ""+qtd * valor;
        SocketClient socketCaixa = new SocketClient();
        SocketClient socketEstoque = new SocketClient();
        String retornoCaixa = socketCaixa.request("acrecentarSaldo;"+valorTotal+";");
        String retornoEstoque = socketEstoque.request("decrese;"+id+";"+qtd+";");
        return false;
    }
}
