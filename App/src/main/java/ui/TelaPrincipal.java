/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import javax.swing.JOptionPane;
import objectValues.Estoque;
import servicos.CaixaService;
import servicos.EstoqueService;

/**
 *
 * @author Marcelo Augusto
 */
public class TelaPrincipal extends javax.swing.JFrame {

    /**
     * Creates new form TelaPrincipal
     */
    EstoqueService estoqueService;
    CaixaService caixaService;

    public TelaPrincipal() {
        estoqueService = new EstoqueService();
        caixaService = new CaixaService();
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        qtdProdutoCadastro = new javax.swing.JTextField();
        nomeProduto = new javax.swing.JTextField();
        qtdProdutosVenda = new javax.swing.JTextField();
        consultarEstoque = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel6 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        comboProdutoEstoque = new javax.swing.JComboBox(estoqueService.listarEstoque().toArray());
        qtdProdutosEstoque = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        btnCadastrarProduto = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        comboProdutosVenda = new javax.swing.JComboBox(estoqueService.listarEstoque().toArray());
        jLabel11 = new javax.swing.JLabel();
        btnVenderProduto = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        valorProduto = new javax.swing.JTextField();
        balancoCaixa = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(900, 550));
        setPreferredSize(new java.awt.Dimension(700, 500));
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Controle de caixa e estoque");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(340, 20, 240, 40);

        jLabel2.setText("Quantidade");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(620, 290, 60, 30);

        jLabel3.setText("Nome");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(40, 160, 30, 30);

        jLabel4.setText("Valor");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(40, 220, 30, 30);
        getContentPane().add(qtdProdutoCadastro);
        qtdProdutoCadastro.setBounds(80, 280, 150, 30);
        getContentPane().add(nomeProduto);
        nomeProduto.setBounds(80, 160, 150, 30);
        getContentPane().add(qtdProdutosVenda);
        qtdProdutosVenda.setBounds(390, 210, 190, 30);

        consultarEstoque.setText("Consultar");
        consultarEstoque.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consultarEstoqueActionPerformed(evt);
            }
        });
        getContentPane().add(consultarEstoque);
        consultarEstoque.setBounds(700, 220, 100, 30);

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        getContentPane().add(jSeparator1);
        jSeparator1.setBounds(610, 80, 10, 390);

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("Cadastrar produto no estoque");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(60, 100, 200, 20);

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);
        getContentPane().add(jSeparator2);
        jSeparator2.setBounds(300, 80, 10, 390);

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setText("Realizar venda");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(400, 100, 100, 20);

        jLabel8.setText("Quantidade");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(10, 280, 60, 30);

        getContentPane().add(comboProdutoEstoque);
        comboProdutoEstoque.setBounds(670, 160, 170, 30);

        qtdProdutosEstoque.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                qtdProdutosEstoqueActionPerformed(evt);
            }
        });
        getContentPane().add(qtdProdutosEstoque);
        qtdProdutosEstoque.setBounds(690, 290, 170, 30);

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel9.setText("Consultar estoque de produtos");
        getContentPane().add(jLabel9);
        jLabel9.setBounds(660, 100, 200, 30);

        btnCadastrarProduto.setText("Cadastrar");
        btnCadastrarProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCadastrarProdutoActionPerformed(evt);
            }
        });
        getContentPane().add(btnCadastrarProduto);
        btnCadastrarProduto.setBounds(100, 350, 100, 30);

        jLabel10.setText("Produto");
        getContentPane().add(jLabel10);
        jLabel10.setBounds(620, 160, 40, 30);

        getContentPane().add(comboProdutosVenda);
        comboProdutosVenda.setBounds(390, 160, 190, 30);

        jLabel11.setText("Quantidade");
        getContentPane().add(jLabel11);
        jLabel11.setBounds(320, 210, 60, 30);

        btnVenderProduto.setText("Vender");
        btnVenderProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVenderProdutoActionPerformed(evt);
            }
        });
        getContentPane().add(btnVenderProduto);
        btnVenderProduto.setBounds(400, 270, 120, 30);

        jLabel12.setText("Produto");
        getContentPane().add(jLabel12);
        jLabel12.setBounds(340, 160, 40, 30);
        getContentPane().add(valorProduto);
        valorProduto.setBounds(80, 220, 150, 30);

        balancoCaixa.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        balancoCaixa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                balancoCaixaActionPerformed(evt);
            }
        });
        getContentPane().add(balancoCaixa);
        balancoCaixa.setBounds(460, 350, 110, 50);

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel13.setText("Balanço do caixa");
        getContentPane().add(jLabel13);
        jLabel13.setBounds(320, 360, 140, 30);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void consultarEstoqueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consultarEstoqueActionPerformed
        Estoque produto = (Estoque) comboProdutoEstoque.getSelectedItem();
        int qtd = estoqueService.getQtdAtualizada(produto.getId());
        qtdProdutosEstoque.setText("" + qtd);
    }//GEN-LAST:event_consultarEstoqueActionPerformed

    private void qtdProdutosEstoqueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_qtdProdutosEstoqueActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_qtdProdutosEstoqueActionPerformed

    private void btnCadastrarProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCadastrarProdutoActionPerformed
        try {
            String nome = nomeProduto.getText();
            double valor = Double.parseDouble(valorProduto.getText());
            int qtd = Integer.parseInt(qtdProdutoCadastro.getText());
            estoqueService.cadastrarProduto(nome, valor, qtd);
            JOptionPane.showMessageDialog(null, "Cadastro realizado!");
            nomeProduto.setText("");
            valorProduto.setText("");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Informe valores com o tipo correto!");
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnCadastrarProdutoActionPerformed

    private void btnVenderProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVenderProdutoActionPerformed
        try {
            Estoque produto = (Estoque) comboProdutosVenda.getSelectedItem();
            int qtd = Integer.parseInt(qtdProdutosVenda.getText());
            if (caixaService.vender(produto.getId(), qtd, produto.getValor())) {
                JOptionPane.showMessageDialog(null, "Venda realizada com sucesso!");
            } else {
                JOptionPane.showMessageDialog(null, "Venda não confirmada!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }//GEN-LAST:event_btnVenderProdutoActionPerformed

    private void balancoCaixaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_balancoCaixaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_balancoCaixaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField balancoCaixa;
    private javax.swing.JButton btnCadastrarProduto;
    private javax.swing.JButton btnVenderProduto;
    private javax.swing.JComboBox comboProdutoEstoque;
    private javax.swing.JComboBox comboProdutosVenda;
    private javax.swing.JButton consultarEstoque;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextField nomeProduto;
    private javax.swing.JTextField qtdProdutoCadastro;
    private javax.swing.JTextField qtdProdutosEstoque;
    private javax.swing.JTextField qtdProdutosVenda;
    private javax.swing.JTextField valorProduto;
    // End of variables declaration//GEN-END:variables
}
