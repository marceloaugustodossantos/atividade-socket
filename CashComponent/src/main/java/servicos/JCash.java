/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicos;

import dao.DaoCaixa;

/**
 *
 * @author Marcelo Augusto
 */
public class JCash implements JCashIF{
    
    DaoCaixa dao = new DaoCaixa();

    @Override
    public double balanco() {
        return dao.consultarBalanco();
    }

    @Override
    public void acrecentarSaldo(double valor) {
        dao.acrecentarSaldo(valor);
    }
    
}
