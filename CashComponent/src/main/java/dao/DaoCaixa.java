package dao;

import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import objectValues.Caixa;

public class DaoCaixa {

    Conexao conexao = new Conexao();

    public double consultarBalanco() {
        try {
            Connection c = conexao.getConnection();
            Statement stat = c.createStatement();
            ResultSet rs = stat.executeQuery("select saldo from tbcash");
            double balanco = 0;
            if (rs.next()) {
                balanco = rs.getDouble(1);
            }
            stat.close();
            c.close();
            return balanco;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    public void acrecentarSaldo(double valor){
        try {
            Connection c = conexao.getConnection();
            Statement stat = c.createStatement();
            stat.executeUpdate("UPDATE tbcash SET saldo = saldo + "+valor);            
            stat.close();
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
