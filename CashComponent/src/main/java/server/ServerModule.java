/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


public class ServerModule {
    
    private ServerSocket server;
    
    public void startServer(){
        
        try {            
            server = new ServerSocket(10102, 100);
            while( true ){
                Socket socket = server.accept();
                ThreadServer t = new ThreadServer(socket);
                t.start();               
            }            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
    }
            
}
