/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import servicos.JStock;
import servicos.JStockIF;

public class ThreadServer extends Thread {

    private Socket socket;
    JStockIF jstock = new JStock();

    public ThreadServer(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            byte[] buffer = new byte[1024];
            this.socket.getInputStream().read(buffer);
            int aux = 0;
            for (int i = 0; i < buffer.length; i++) {
                if (buffer[i] != 0) {
                    aux++;
                }
            }

            byte[] bufferAux = new byte[aux];
            System.arraycopy(buffer, 0, bufferAux, 0, bufferAux.length);
            String request = new String(bufferAux);

            String[] protocol = request.split(";");//quebra o protocolo
            String key = protocol[0]; //recupera a chave do protocolo

            if (key.equalsIgnoreCase("listAll")) {
                String[] estoque = jstock.listAll();
                response(String.valueOf(estoque), socket);
            } else if (key.equalsIgnoreCase("decrese")) {
                int id = Integer.valueOf(protocol[1]);
                int qtd = Integer.valueOf(protocol[2]);
                jstock.decrese(id, qtd);
            }
            this.socket.close();
        } catch (IOException ex) {
            Logger.getLogger(ThreadServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void response(String response, Socket socket) {
        try {
            socket.getOutputStream().write(response.getBytes());
            socket.shutdownOutput();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
