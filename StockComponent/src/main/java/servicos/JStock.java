package servicos;

import java.util.List;

import dao.DaoEstoque;
import objectValues.Estoque;

public class JStock implements JStockIF {

    DaoEstoque dao = new DaoEstoque();

    public String[] listAll() {
        List<Estoque> listaEstoque = dao.listarEstoque();
        String[] arrayString = new String[1024];
        for (int i = 0; i < listaEstoque.size(); i++) {
            String estoqueString = listaEstoque.get(i).toString();
            arrayString[i] = estoqueString;
        }
        return arrayString;
    }

    @Override
    public void decrese(int id, int qtd) {
        boolean update = dao.decrementarEstoque(id, qtd);
    }
    
    
}
