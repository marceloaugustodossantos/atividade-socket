package objectValues;

public class Estoque {
	private int id;
	private String nome;
	private double valor;
	private double quantidade;
	
	public Estoque(){
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public double getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(double quantidade) {
		this.quantidade = quantidade;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "C�digo: " + this.id + " \n Nome: " +this.nome +"\n Quantidade: " + this.quantidade +" \n valor: "+this.valor ;
	}
	
	
	
}
