package dao;

import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import objectValues.Estoque;

public class DaoEstoque {

    Conexao conexao = new Conexao();

    public List<Estoque> listarEstoque() {
        try {
            Connection c = conexao.getConnection();
            Statement stat = c.createStatement();
            ResultSet rs = stat.executeQuery("select * from tbstok");
            List<Estoque> listaEstoque = new ArrayList();
            while (rs.next()) {
                Estoque estoque = new Estoque();
                estoque.setId(rs.getInt("code"));
                estoque.setNome(rs.getString("name"));
                estoque.setQuantidade(rs.getDouble("quantity"));
                estoque.setValor(rs.getDouble("price"));
                listaEstoque.add(estoque);
            }

            stat.close();
            c.close();
            return listaEstoque;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public boolean decrementarEstoque(int id, int qtd){
        try {
            Connection c = conexao.getConnection();
            PreparedStatement stat = c.prepareStatement("UPDATE tbstock SET quantidade = ? WHERE id = ?");
            stat.setInt(1, qtd);
            stat.setInt(2, id);
            int update = stat.executeUpdate();
            stat.close();
            c.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
}
