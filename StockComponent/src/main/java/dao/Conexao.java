package dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class Conexao {
	private String url = "jdbc:postgresql://localhost:5432/dbpod";
    private String usuario = "postgres";
    private String senha = "123456";
    
    public Connection getConnection() throws Exception{
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager.getConnection(url, usuario, senha);
            return c;
        }
        catch(Exception e){
            throw new Exception();
        }
    }
}
